# Data Science Projects

A collection of smaller projects tackling diverse topics such as naive bayes spam filter
implementation, predicting bike rental counts, predicting house sale prices, business analysis,
database design, employee exit surveys, earnings analysis based on college majors, and others.
Projects involve extensive data cleaning, exploratory data analysis, and predictive modelling using python and R. Main
libraries used in python are numpy, pandas, re, and matplotlib, sqlite3, sklearn, scipy while the R code relies on
dplyr, stringr, tidyr, and ggplot2 libraries.
