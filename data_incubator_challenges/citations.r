library(hash)
options(digits = 10)
x = read.csv("data_incubator_challenges/Parking_Citations.csv")
x["ViolDate"][,1] = as.Date(x["ViolDate"][,1], format = "%m/%d/%Y")
filter = x$ViolDate < as.Date("01/01/2019", format = "%m/%d/%Y")
x = x[filter,]

# finding mean violation fine

s1 = mean(na.omit(x$ViolFine))
print(paste("Mean fine is ", s1))

# 81s percentile of vehicles with open fines

s2 = quantile(x["OpenFine"][x["OpenFine"] != 0], 0.81, na.rm = T)
print(paste("81st percentile is ", s2))

# Mean violation fine for each district and maximum out of those

x$PoliceDistrict = gsub("noth", "north", tolower(x$PoliceDistrict))
districts = unique(x$PoliceDistrict)
districts = na.omit(districts)
districts = districts[2:length(districts)]
district_means = hash()

for (i in districts){
  district_means[i] = mean(x$ViolFine[x["PoliceDistrict"] == i], na.rm = T)
}

s3 = max(values(district_means))
print(paste("Highest mean violation fine is ", s3, "in", keys(district_means)[which.max(values(district_means))]))

# Vehicles frequency table
head(sort(table(x$Make), decreasing=T), 50)

x$Make = gsub("MAZ.*", "MAZDA", x$Make)
x$Make = gsub("TOY.*", "TOYOTA", x$Make)
x$Make = gsub("NIS.*", "NISSAN", x$Make)
x$Make = gsub("ACU.*", "ACURA", x$Make)
x$Make = gsub("HYU.*", "HYUND", x$Make)
x$Make = gsub("VOL.*", "VOLKS", x$Make)
x$Make = gsub("LEX.*", "LEXUS", x$Make)
x$Make = gsub("HON.*", "HONDA", x$Make)
x$Make = gsub("DOD.*", "DODGE", x$Make)
x$Make = gsub("CHE.*", "CHEV", x$Make)

freq_table = hash(head(sort(table(x$Make), decreasing=T), 10))

sum_japanese = values(freq_table["ACURA"]) + values(freq_table["HONDA"]) + values(freq_table["NISSAN"]) + values(freq_table["TOYOTA"])
proportion = sum_japanese / length(x$Make)

print(paste("In top 10 there are 4 brands of Japanese-made vehicles and their proportion in all citations is", proportion))

# Linear Regression
citations_per_year = hash(table(substring(x$ViolDate, 1, 4)))
citations_per_year

years = integer(0)
cit = integer(0)
j = 1

for (i in seq(2004, 2014)){
 years[j] = i
 cit[j] = values(citations_per_year[toString(i)])
 j = j + 1
}

fit = lm(cit ~ years)
s5 = summary(fit)$coefficients[2]

print(paste("The slope of the linear fit is", s5))

# Ratio of parking fines and auto thefts in each district for 2015
x1 = read.csv("data_incubator_challenges/BPD_Part_1_Victim_Based_Crime_Data.csv")
x1_2015 = x1[substring(x1$CrimeDate, 7, 10) == "2015",]
x1_2015_thefts = x1_2015[x1_2015["Description"] == "AUTO THEFT",]
x_2015 = x[substring(x$ViolDate, 1, 4) == "2015",]
x_2015$Description = gsub(".*Park.*", "Parking", x_2015$Description)
x_2015_parking = x_2015[x_2015["Description"] == "Parking",]

parking_ft = table(x_2015_parking$PoliceDistrict)
thefts_ft = table(x1_2015_thefts$District)
parking_ft = parking_ft[2:length(parking_ft)]
thefts_ft = thefts_ft[-9]
ratio = thefts_ft / parking_ft
s6 = max(ratio)

print(paste("Highest ratio of car thefts to parking tickets is in northwestern district and it amounts to", s6))
