import numpy as np

# Function which determines number of steps in the shortest path given 
# the knight type (a, b) and grid type (nxn), constructing a graph
# considering all possible states without repeating the states
def shortest_path(a, b, n):
    start = [0, 0]
    end = [n-1, n-1]
    possible_steps = [a, b, -a, -b]
    moves = []
    for i in possible_steps:
        for j in possible_steps:
            if abs(i) != abs(j):
                moves.append((i,j))
    moves = np.array(moves)

    current_state = np.array(start)
    tot_steps = 0
    visited_states = np.array([start])
    while end not in current_state.tolist():
        temporary_state = np.empty((1,2), dtype = int)
        for i in xrange(current_state.shape[0]):
            temporary_state = np.concatenate((temporary_state, current_state[i] + moves), 0) 
        current_state = np.delete(temporary_state, 0, 0)
        tot_steps += 1
        out_of_bounds = []
        repeated = []
        idx = 0
        for i in xrange(current_state.shape[0]):
            if (np.any(current_state[i] < 0)) or (np.any(current_state[i] > n-1)):
                out_of_bounds.append(idx) 
            if (current_state[i].tolist() in visited_states.tolist()):
                repeated.append(idx)
            idx += 1
        mask = out_of_bounds + repeated
        current_state = np.delete(current_state, mask, 0)
        if current_state.size != 0:
            current_state = np.unique(current_state, axis = 0)
        else:
            return "Bad Knight"
        visited_states = np.concatenate((visited_states, current_state), 0)
        print(tot_steps)
    return tot_steps

#solution1 = shortest_path(1, 2, 5)
#print(solution1)
#solution2 = shortest_path(4, 7, 25)
#print(solution2)
solution3 = shortest_path(13, 23, 1000)
print(solution3)
#solution4 = shortest_path(73, 101, 10000)
#print(solution4)

def knight_profiling(n):
    bad_knight_count = 0
    sum_of_shortest_paths = 0
    for i in range(1, n):
        for j in range(i, n):
            if i == j:
                if (n-1) % i == 0:
                    sum_of_shortest_paths += ((n-1) / i)
                else:
                    bad_knight_count += 1
            else:
                result = shortest_path(i, j, n)
                if result == "Bad Knight":
                    bad_knight_count += 1
                else:
                    sum_of_shortest_paths += result
    return bad_knight_count, sum_of_shortest_paths

#s5 = knight_profiling(5)
#print(s5)
#s25 = knight_profiling(25)
#print(s25)
